package assignment_1;

import org.apache.hadoop.conf.Configuration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    private static final String LOCAL_SOURCE = "EmployeeDetails";
    private static final String HDFS_DESTINATION = "Employees";


    public static void main(String[] args) throws IOException {
        generate100CSVFiles();

        // HDFS Configurations and make directory in HDFS
        String hdfsPath = "hdfs://" + "localhost:9000";
        Configuration conf = new Configuration();
        conf.set("fs.default.name", hdfsPath);
        HDFSUtil.makeDirHDFS(conf, "Employee");

        HDFSUtil.copyAllFilesFromLocalToHDFS(conf, LOCAL_SOURCE, HDFS_DESTINATION);

        HBaseUtils hbase = new HBaseUtils();
        hbase.insertCSVDataToHbase(HDFS_DESTINATION, conf);


    }

    public static void generate100CSVFiles() throws IOException {
        File employeesCSVDir = new File("EmployeeDetails");
        if (!employeesCSVDir.exists()) {
            boolean dirMade = employeesCSVDir.mkdir();
            System.out.println("LOG_INFO: Generated Directory at " + employeesCSVDir.getAbsolutePath() + ": " + dirMade + "\n");
        }

        for (int i = 1; i <= 100; i++) {
            String fileName = LOCAL_SOURCE + "/Employee_details_" + i + ".csv";
            FileWriter csvWriter = new FileWriter(fileName);

            // Headers
            csvWriter.append("Id");
            csvWriter.append(",");
            csvWriter.append("Name");
            csvWriter.append(",");
            csvWriter.append("Company");
            csvWriter.append(",");
            csvWriter.append("Address");
            csvWriter.append(",");
            csvWriter.append("Building Code");
            csvWriter.append(",");
            csvWriter.append("Age");
            csvWriter.append(",");
            csvWriter.append("Phone");
            csvWriter.append("\n");

            for (int j = 0; j < 10; j++) {
                Employee employee = new Employee();
                csvWriter.append(employee.generateObjectCSV());
                csvWriter.append("\n");
            }

            csvWriter.flush();
            csvWriter.close();
        }
    }
}
